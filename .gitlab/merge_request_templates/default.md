### What is in this MR

- E.g. a short explanation of what this MR does.

### How to test this MR

- E.g. a short series of steps on how to test the features/bug fixes in this MR.

### Merge Request Checklist

- [ ] Test locally with `npm run dev`
- [ ] Test locally with `npx install-gitapi` in a separate folder
