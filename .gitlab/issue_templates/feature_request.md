## Feature description

<!-- What is the desired new feature !-->

## Examples

<!-- Are there any examples of this which exist in other software? !-->

/label ~"needs::feedback" ~"feature::request"
