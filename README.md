#  <img src="/statics/gitapi-logo.png"  width="60" height="60" style="display: inline; margin-bottom: -.5em; margin-right: .3em"> GITAPI - NPX INSTALLER

![](https://img.shields.io/npm/v/install-gitapi) ![](https://img.shields.io/gitlab/license/45363276) ![](https://img.shields.io/bundlephobia/min/install-gitapi) ![](https://img.shields.io/gitlab/issues/open-raw/45363276) ![](https://img.shields.io/librariesio/release/npm/install-gitapi)

Npx installer for [Gitapi](https://gitlab.com/multi-coop/experimental/gitapi-draft)

---

An open source project by the tech coop [multi](https://multi.coop)

---

## What does it do ?

This npm package working with `npx` allows you to set up Gitapi in your current working directory (cwd). 

It clones the latest version of Gitapi from its repo and add it into your cwd.

---

## Use

```bash
npx install-gitapi
```

<br>

### Before `npx install-gitapi`

Imagine you have a project folder as follows :

```
.
├── . (my project directory)
│   ├── my-data
|   |   ├── csv
|   |   |   └── some_table.csv
|   |   └── json
|   |       └── some_json.json
│   ├── .gitignore
│   ├── my-script.py
│   └── README.md
```

<br>

### After `npx install-gitapi && npm i`

```
.
├── . (my project directory)
│   ├── my-data
|   |   ├── csv
|   |   |   └── some_table.csv
|   |   └── json
|   |       └── some_json.json
│   ├── .gitignore              <-- don't forget to add /node_modules
│   ├── my-script.py
│   ├── README.md
│   |
│   |   ⬆️ Your files
│   |  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
│   |   ⬇️ Gitapi files
│   |
│   ├── config-gitapi.yaml     <-- you can adapt it to your data folder (./my-data)
│   ├── data-test              <-- you can delete once you adapted config-gitapi.yaml
|   |   ├── csv
|   |   |   └── test.csv
|   |   └── json
|   |       └── test-schema.json
│   ├── dist
|   |   └── index.html
│   ├── example.env            <-- you can copy it into your own .env file (optional)
│   ├── gitapi
|   |   ├── api.js
|   |   ├── config
|   |   ├── controllers
|   |   ├── routes
|   |   ├── services
|   |   └── utils
│   ├── gitapi-local.js
│   ├── LICENSE_GITAPI
│   ├── netlify.toml
│   ├── node_modules         <-- created after npm i
|   |   └── ...
│   ├── LICENSE_GITAPI
│   ├── package-lock.json    <-- created after npm i
│   ├── package.json
│   └── README_GITAPI.json
```

---

## NPM

Published on NPM [here](https://www.npmjs.com/package/install-gitapi)

---

### References

- https://blog.shahednasser.com/how-to-create-a-npx-tool/
- https://dev.to/wuz/setting-up-a-npx-username-card-1pip
- https://github.com/nuxt/create-nuxt-app/blob/master/packages/create-nuxt-app/lib/cli.js
- https://blog.jskoneczny.pl/post/run-git-commands-from-node-js-application-using-javascript