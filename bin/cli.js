#! /usr/bin/env node

const path = require('path');
const cac = require('cac');
const fs = require('fs');
// const fsExtra = require('fs-extra');
const cmd = require('./cmd');

const readline = require('readline');

const { name, version, gitapiRepo } = require('../package.json')
const package = name;

console.log(`\nHello ! Welcome to Gitapi v${version} installer !`);

const env = process.env.NODE_ENV
const dev = env === 'dev'

// const generator = path.resolve(__dirname, './')
// console.log('generator :', generator);

const cli = cac('install-gitapi')

const showEnvInfo = async () => {
  console.log('\nEnvironment Info:')
  const result = await envinfo
    .run({
      System: ['OS', 'CPU'],
      Binaries: ['Node', 'Yarn', 'npm'],
      Browsers: ['Chrome', 'Edge', 'Firefox', 'Safari'],
    })
  console.log(result)
  process.exit(1)
}

function askQuestion(query) {
  const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
  });

  return new Promise(resolve => rl.question(query, ans => {
      rl.close();
      resolve(ans);
  }))
}

async function run () {
  await cli
    .command('[out-dir]', 'Generate in a custom directory or current directory')
    .option('-i, --info', 'Print out debugging information relating to the local environment')
    .option('--verbose', 'Show debug logs')
    .action(async (outDir = '.', cliOptions) => {
      if (cliOptions.info) {
        return showEnvInfo()
      }
      const tempDir = 'gitapi_temp'

      console.log()
      console.log(`${package} v${version}\n`)
      console.log(`${package} > outDir : ${outDir}`)
      console.log(`${package} > env : ${env}\n`)

      const { verbose } = cliOptions
      const cwd = process.cwd()
      console.log(`${package} > verbose :`, verbose)
      console.log(`${package} > cwd :`, cwd )
      
      const baseDir = outDir === '.' ? path.basename(process.cwd()) : outDir
      console.log(`${package} > baseDir : ${baseDir}`)
      console.log()

      // As questions to user - confirm
      const continueProcess = await askQuestion(
        `${package} > Are you sure you want to install Gitapi ? y/n (default: y) `);
      if (continueProcess === 'n') {
        console.log(`${package} > process.exit(1)`)
        console.log(`${package} > bye !n\n`)
        process.exit(1)
      }

      console.log(`\n✨  Copying Gitapi project in : ${baseDir}\n`)

      // Ask questions to user - target directory
      const defaultTargetPath = './' 
      let userTargetDirectory = await askQuestion(
        `${package} > Where do you want to copy Gitapi inside this directory ? (default : ./ ) `) || defaultTargetPath;
      // sanitize defaultTargetPath string
      const isDefault = userTargetDirectory === defaultTargetPath
      const startsWithDot = userTargetDirectory.startsWith('.')
      const startsWithSlash = userTargetDirectory.startsWith('/')
      const endsWithSlash = userTargetDirectory.endsWith('/')
      if (!isDefault && !startsWithDot) {
        const addRoot = startsWithSlash ? '.' : './'
        userTargetDirectory = `${addRoot}${userTargetDirectory}`
      }
      if (!isDefault && endsWithSlash) {
        userTargetDirectory = userTargetDirectory.slice(0, -1)
      }
      if (env === 'dev') {
        console.log(`${package} > (dev) user's path : ${userTargetDirectory}` )
        console.log(`${package} > (dev) copying to : ./${tempDir}` )
        
      } else {
        console.log(`${package} > copying to : ${userTargetDirectory} ${env === 'dev' ? '(dev)' : ''}` )
      }
      console.log()

      // Ask questions to user - has data directory
      const hasDataDir = await askQuestion(
        `${package} > Do you already have data folder ? y/n (default: y) ? `) || 'y' ;
      const hasData = hasDataDir === 'y'
      console.log(`${package} > hasData :`, hasData )
      console.log()

      // Ask questions to user - has config yaml file
      const hasConfigGitapi = await askQuestion(
        `${package} > Do you already have a 'config-gitapi.yml' file ? y/n (default: n) ? `) || 'n' ;
      const hasYaml = hasConfigGitapi === 'y'
      console.log(`${package} > hasYaml :`, hasYaml )
      console.log()

      // Ask questions to user - has package json file
      const hasPackageJson = await askQuestion(
        `${package} > Do you already have a 'package.json' file ? y/n (default: n) ? `) || 'n' ;
      const hasPackage = hasPackageJson === 'y'
      console.log(`${package} > hasPackage :`, hasPackage )
      console.log()

      // Prepare paths
      const tempDirTarget = env === 'dev' ? 'gitapi_target' : userTargetDirectory
      const tempDirPath = `./${tempDir}`
      const tempDirPathTarget = env === 'dev' ? `./${tempDirTarget}` : userTargetDirectory

      console.log(`${package} > tempDirPath : ${tempDirPath}`)
      console.log(`${package} > tempDirPathTarget : ${tempDirPathTarget}`)

      // ------------ //
      // Run commands //
      // ------------ //

      // Remove / create temp folder
      if (fs.existsSync(tempDirPath)) {
        fs.rmSync(tempDirPath, {recursive: true})
      }
      if (dev && fs.existsSync(tempDirPathTarget)) {
        fs.rmSync(tempDirPathTarget, {recursive: true})
      }

      // Clone repo in temp folder
      await cmd.clone(tempDirPath, gitapiRepo)
      await cmd.execute(`ls -a -l ${tempDirPath}`, true)
      console.log(`${package} > repo cloned in ${tempDirPath}`)

      // Remove unecessary folders and files
      const toRemove = [
        { path: 'data', type: 'dir' },
        { path: '.git', type: 'dir' },
        { path: '.gitignore', type: 'file' },
        { path: 'package-lock.json', type: 'file' },
        { path: 'config-gitapi.yaml', type: 'file' },
      ]
      toRemove.map(async (tr) => {
        const trPath = `${tempDirPath}/${tr.path}`
        switch (tr.type) {
          case 'dir':
            fs.rmSync(trPath, {recursive: true})
            // await fs.unlink(`${trPath}`)
            break
          case 'file':
            await cmd.execute(`rm ${trPath}`)
            break
          }
      })

      // Rename some files
      const toRename = [
        { path: 'README.md', newName: 'README_GITAPI.md' },
        { path: 'LICENSE', newName: 'LICENSE_GITAPI' },
        // { path: 'data', newName: 'data-test' },
      ]
      toRename.map(async (tr) => {
        const trPathOld = `${tempDirPath}/${tr.path}`
        const trPathNew = `${tempDirPath}/${tr.newName}`
        await cmd.execute(`mv ${trPathOld} ${trPathNew}`)
      })

      // Move gitapi scripts and folders in parent target directory
      console.log(`\n${package} > dev : ${dev}`)
      console.log(`${package} > tempDirPathTarget : ${tempDirPathTarget}\n`)

      const modelData = `./${tempDir}/install-gitapi-files/data-test`
      const modelConfig = `./${tempDir}/install-gitapi-files/install-config-gitapi.yaml`
      let modelDataTo
      let modelConfigTo
      let modelRemove

      // Only copy package.json file if user said so
      if (hasPackage) {
        const userPackage = `./${env === 'dev' ? 'dev-' : '' }package.json`
        console.log(`\n${package} > hasPackage : ${hasPackage}`)
        const modelPackage = `./${tempDir}/package.json`
        console.log(`${package} > modelPackage : ${modelPackage}`)

        // As questions to user - confirm we update his/her own package.json 
        console.log(`${package} > userPackage :`, userPackage )
        console.log(`${package} > modelPackage :`, modelPackage )
        console.log()

        // read gitapi package.json
        const rawGitapiJson = fs.readFileSync(modelPackage)
        const gitapiJson = JSON.parse(rawGitapiJson)
        console.log(`\n${package} > gitapiJson :\n`, gitapiJson )
        // remove gitapi json - not more use
        await cmd.execute(`rm ${modelPackage}`)
        console.log()

        // read user package.json
        let userJson = {}
        if (fs.existsSync(userPackage)) {
          const rawUserJson = fs.readFileSync(userPackage)
          userJson = JSON.parse(rawUserJson)
        }
        console.log(`\n${package} > userJson :\n`, userJson )
        console.log()

        // mix both of them
        const keysFromGitapiJsonToLoop = [
          { name: 'main',             type: 'string', do: 'copy' },
          { name: 'dependencies',     type: 'object', do: 'loop' },
          { name: 'scripts',          type: 'object', do: 'loop' },
          { name: 'repository',       type: 'object', do: 'copy' },
          { name: 'keywords',         type: 'array',  do: 'loop' },
          { name: 'license',          type: 'string', do: 'copy' },
          { name: 'devDependencies',  type: 'object', do: 'loop' },
        ]
        keysFromGitapiJsonToLoop.forEach(key => {
          const kDo = key.do
          const kType = key.type
          const kName = key.name
          const gitapiKey = gitapiJson[kName]
          console.log(`\n... "${kName}" / ${kType} / gitapiKey : \n`, gitapiKey)
          const uKey = userJson[kName]
          if (!uKey) { 
            userJson[kName] = gitapiKey
          } else {
            switch(kDo) {
              case 'copy':
                userJson[kName] = gitapiKey
                break
              case 'loop':
                if (kType === 'array') {
                  const tempArr = [ ...userJson[kName] ]
                  gitapiKey.forEach(item => { 
                    !tempArr.includes(item) && tempArr.push(item)
                  })
                  userJson[kName] = tempArr
                }
                if (kType === 'object') {
                  const tempObj = { ...userJson[kName] }
                  for (const k in gitapiKey) {
                    const gitapiVal = gitapiKey[k]
                    tempObj[k] = gitapiVal
                  }
                  userJson[kName] = tempObj
                }
                break
            }
          }
          console.log(`... userJson[kName] : \n`, userJson[kName])
        })
        console.log(`\n${package} > userJson :\n`, userJson )
        console.log()

        // Confirm
        const updatePackage = await askQuestion(
          `${package} > Confirm you want to update your own '${userPackage}' file ? y/n (default: n) ? `) || 'n' ;
        const confirmUpdatePackage = updatePackage === 'y'
        console.log(`${package} > hasPackage :`, confirmUpdatePackage )
        // save mixed package and update user's json
        const userJsonData = JSON.stringify(userJson, null, 2)
        fs.writeFileSync(userPackage, userJsonData)
      }

      if (dev) {
        modelDataTo = `./${tempDirTarget}/data-test`
        modelConfigTo = `./${tempDirTarget}/config-gitapi.yaml`
        modelRemove = `./${tempDirTarget}/install-gitapi-files`

        // Remove / create temp folder
        if (fs.existsSync(tempDirPathTarget)) {
          fs.rmSync(tempDirPathTarget, {recursive: true})
        }
        if (fs.existsSync(modelDataTo)) {
          fs.rmSync(modelDataTo, {recursive: true})
        }

        // Push to target folder
        await cmd.execute(`mkdir ${tempDirPathTarget}`)
        !hasDataDir && await cmd.execute(`mkdir ${modelDataTo}`)

        await cmd.execute(`cp -a ${tempDirPath}/. ${tempDirPathTarget}/`)
        
      } else {
        const addon = userTargetDirectory === './' ? '' : '/'
        modelDataTo = `${userTargetDirectory}${addon}data-test`
        modelConfigTo = `${userTargetDirectory}${addon}config-gitapi.yaml`
        modelRemove = `${userTargetDirectory}${addon}install-gitapi-files`

        // Push to target folder
        await cmd.execute(`cp -a ${tempDirPath}/. ${userTargetDirectory}`)
      }

      // Only copy model data directory if necessary
      if (!hasDataDir) {
        await cmd.execute(`cp -a ${modelData}/. ${modelDataTo}`)
      }

      // Only copy config-gitapi.yaml file if user said so
      if (!hasYaml) {
        await cmd.execute(`mv ${modelConfig} ${modelConfigTo}`)
      }
      await cmd.execute(`rm -r ${tempDirPath}`)
      await cmd.execute(`rm -r ${modelRemove}`)

      const dirFinish = env === 'dev' ? tempDirPathTarget : userTargetDirectory
      const envFinish = env === 'dev' ? `(${env}) ` : ''
      console.log(`\n✨ Gitapi is now installed in the directory ${envFinish}: ${dirFinish} !\n`)
      console.log(`Now you can run : npm install && npm run dev`)
      console.log(`and check : http://localhost:3000\n`)
    })

  cli.help()
  cli.version(version)
  cli.parse()
}

try {
  run()
} catch (err) {
  // https://github.com/cacjs/cac/blob/f51fc2254d7ea30b4faea76f69f52fe291811e4f/src/utils.ts#L152
  // https://github.com/cacjs/cac/blob/f51fc2254d7ea30b4faea76f69f52fe291811e4f/src/Command.ts#L258
  if (err.name === 'CACError' && err.message.startsWith('Unknown option')) {
    console.error()
    console.error(err.message)
    console.error()
    cli.outputHelp()
  } else {
    console.error()
    console.error(err)
  }
  process.exit(1)
}
