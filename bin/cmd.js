const util = require('util');
const fs = require('fs');
const path = require('path');
// const { exec, execSync, spawn } = require('child_process');
const execAsync = util.promisify(require('child_process').exec);
const simpleGit = require("simple-git");
const git = simpleGit.default();

const { name } = require('../package.json')
const package = name;

async function execute(cmd, std = false) {
  console.log(`${package} > execute cmd : ${cmd}`)
  const { stdout, stderr } = await execAsync(cmd);
  // await execAsync(cmd);
  std && console.log(stdout);
  // console.log('stdout:\n\n', stdout);
  // console.log('stderr:\n\n', stderr);
  // console.log()
}

const copyRecursiveSync = function(src, dest) {
  const exists = fs.existsSync(src);
  const stats = exists && fs.statSync(src);
  const isDirectory = exists && stats.isDirectory();
  if (isDirectory) {
    fs.mkdirSync(dest);
    fs.readdirSync(src).forEach(function(childItemName) {
      copyRecursiveSync(path.join(src, childItemName),
                        path.join(dest, childItemName));
    });
  } else {
    fs.copyFileSync(src, dest);
  }
};

async function clone(dir, repoUrl) {
  await git.clone(repoUrl, dir);
}

module.exports = {
  execute,
  clone,
  copyRecursiveSync
}